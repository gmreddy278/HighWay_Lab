package com.highway.us.HighwayLab;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class USRouteTest {
	
	private Route route;
	private RouteFactory routeFactory;
	
	@Before
	public void setUp() {
		
		routeFactory = new RouteFactory();
		route = routeFactory.getRoute("US95");
	}

	
	@Test
	public void testUSWest() {

		routeFactory = new RouteFactory();
		route = routeFactory.getRoute("US95");
		assertEquals("West", route.getRouteLocation());
	}
	
	@Test
	public void testUSEast() {

		routeFactory = new RouteFactory();
		route = routeFactory.getRoute("US5");
		assertEquals("East", route.getRouteLocation());
	}
	
	@Test
	public void testUsNorth() {

		routeFactory = new RouteFactory();
		route = routeFactory.getRoute("US2");
		assertEquals("North", route.getRouteLocation());
	}
	
	@Test
	public void testUSSouth() {

		routeFactory = new RouteFactory();
		route = routeFactory.getRoute("US52");
		assertEquals("South", route.getRouteLocation());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalArgumentException() {
		
		routeFactory = new RouteFactory();
		route = routeFactory.getRoute("US152");
		route.getRouteLocation();
		
	}
	
	

}
