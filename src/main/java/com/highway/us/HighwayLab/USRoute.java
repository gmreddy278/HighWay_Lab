package com.highway.us.HighwayLab;

public class USRoute extends Route{

	public USRoute() {
		// TODO Auto-generated constructor stub
	}
	
	public USRoute(String routeNumber) {
		super.setRouteNumber(routeNumber);
	}
	
	@Override
	public String getRouteLocation() {
		int number = Integer.parseInt(getRouteNumber());
		if(number<1 || number >99) {
			throw new IllegalArgumentException();
		}
		else if(((number%2) != 0) && number<50 ) {
			return "East";
		}
		else if(((number%2) != 0) && number>=50 ) {
			return "West";
		}
		else if(((number%2) == 0) && number<50 ) {
			return "North";
		}
		else if(((number%2) == 0) && number>=50 ) {
			return "South";
		}
		else return null;
	}

	
}
