package com.highway.us.HighwayLab;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class InterstateTest {
	
	private Route route;
	private RouteFactory routeFactory;
	
	@Before
	public void setUp() {
		
		routeFactory = new RouteFactory();
		route = routeFactory.getRoute("I95");
	}

	@Test
	public void testIntersateEast() {
		routeFactory = new RouteFactory();
		route = routeFactory.getRoute("I95");
		assertEquals("East", route.getRouteLocation());
	}
	
	@Test
	public void testIntersateWest() {
		routeFactory = new RouteFactory();
		route = routeFactory.getRoute("I5");
		assertEquals("West", route.getRouteLocation());
	}
	
	@Test
	public void testIntersateSouth() {
		routeFactory = new RouteFactory();
		route = routeFactory.getRoute("I2");
		assertEquals("South", route.getRouteLocation());
	}
	
	@Test
	public void testIntersateNorth() {
		routeFactory = new RouteFactory();
		route = routeFactory.getRoute("I52");
		assertEquals("North", route.getRouteLocation());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalArgumentException() {
		
		routeFactory = new RouteFactory();
		route = routeFactory.getRoute("I152");
		route.getRouteLocation();
	}
	
	

}
