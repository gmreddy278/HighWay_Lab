package com.highway.us.HighwayLab;

public class USHighwayClient {

	public static void main(String[] args) {

		RouteFactory routeFactory = new RouteFactory();

		System.out.println("US Route");

		System.out.println("*************************");

		Route route1 = routeFactory.getRoute("US1");
		System.out.println(" US1 : "+route1.getRouteLocation());

		Route route2 = routeFactory.getRoute("US99");
		System.out.println(" US99 : "+route2.getRouteLocation());

		Route route3 = routeFactory.getRoute("US2");
		System.out.println(" US2 : "+route3.getRouteLocation());

		Route route4 = routeFactory.getRoute("US98");
		System.out.println(" US98 : "+route4.getRouteLocation());

		System.out.println("Interstate");

		System.out.println("*************************");

		Route route5 = routeFactory.getRoute("I5");
		System.out.println(" I5 : "+route5.getRouteLocation());

		Route route6 = routeFactory.getRoute("I99");
		System.out.println(" I99 : "+route6.getRouteLocation());

		Route route7 = routeFactory.getRoute("I2");
		System.out.println(" I2 : "+route7.getRouteLocation());

		Route route8 = routeFactory.getRoute("I98");
		System.out.println(" I98 : "+route8.getRouteLocation());
	}

}
