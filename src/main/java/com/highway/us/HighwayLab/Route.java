package com.highway.us.HighwayLab;


//enum Orientations {EAST_WEST, NORTH_SOUTH}
public abstract class Route {
	
	// EAST_WEST - EVEN
	// NORTH_SOUTH - ODD
	private String routeNumber;
	//private Orientations orientations;
	
	public String getRouteNumber() {
		return routeNumber;
	}
	public void setRouteNumber(String routeNumbers) {
		this.routeNumber = routeNumbers;
	}
	/*public Orientations getOrientations() {
		return orientations;
	}
	public void setOrientations(Orientations orientations) {
		this.orientations = orientations;
	}*/
	
	
	public abstract String getRouteLocation();
	

}
