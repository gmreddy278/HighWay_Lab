package com.highway.us.HighwayLab;

public class RouteFactory {

	public Route getRoute(String routeType) {
		if (routeType == null) {
			return null;
		}
		if (routeType.startsWith("I")) {
			return new InterstateRoute(routeType.substring(1));

		} else if (routeType.startsWith("US")) {
			return new USRoute(routeType.substring(2));

		}

		return null;
	}

}
